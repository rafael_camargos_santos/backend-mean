# Backend Swapped (MEAN - MongoDB, Express.js, Angular.js, Node.js)

## Instalação

Dê clone no repositório e execute `npm install` na pasta do projeto para 
instalar as dependências

## Rodando o Banco de Dados

### Instale o MongoDB

https://docs.mongodb.org/manual/tutorial/install-mongodb-on-os-x/
com Homebrew
    brew update
    brew install mongodb

### Rode o mongo

    sudo mkdir -p /data/db

#### na pasta /usr/local/Cellar/Mongodb
    sudo mongod
#### rode o mongo shell (da mesma pasta):
    ./bin/mongo
#### crie o banco local do projeto
    use swapped
    db.test.save({nome:"teste"})

## Rodando o App localmente

Na pasta do projeto, execute:

    node server.js

## Arquitetura do projeto
    
    server.js           --> Configuração do server (dependências, endpoints...)
    package.json        --> Configuração de dependências a serem instaladas pelo npm
    public/             --> arquivos do frontend de testes (blog)
      css/              --> css files
        app.css         --> default stylesheet
      img/              --> image files
      js/               --> javascript files
        app.js          --> declaração do app em Angular.js
        controllers.js  --> application controllers
        directives.js   --> custom angular directives
        filters.js      --> custom angular filters
        services.js     --> custom angular services
        lib/            --> angular and 3rd party JavaScript libraries
          angular/
            angular.js            --> the latest angular js
            angular.min.js        --> the latest minified angular js
            angular-*.js          --> angular add-on modules
            version.txt           --> version number
    routes/
      api.js            --> descrição das funcionalidades dos endpoints
      index.js          --> route for serving HTML pages and partials
      env.js            --> seta mongoURI dependendo do ambiente
    views/
      index.jade        --> main page for app
      layout.jade       --> doctype, title, head boilerplate
      partials/         --> angular view partials (partial jade templates)
        partial1.jade
        partial2.jade



## Editando Endpoints

Para editar os endpoints altere dois arquivos: api.js (adicione a funcionalidade do endpoint) e 
server.js (adicionando endereço do endpoint e linkando o método no JSON API)

## Configurações AWS

### Credenciais MongoDB

admins:

  User: root
  Password: SSSvGALha2Br

  User: appsimples
  Password: amazonapp123654

banco backendBootstrap:

  User: backendBootstrap
  Password: amazonapp123654

### reiniciar mongo

na pasta stack:
    sudo bash ctlscript.sh restart mongodb

### Public DNS

ec2-52-90-169-151.compute-1.amazonaws.com
(muda toda vez que reiniciamos o server na AWS)

### Para acessar o server via SSH

(só na primeira vez):
    chmod 400 *path para projeto*/BackendBootstrap/resources/BackendBootstrap.pem

    ssh -i *path para projeto*/BackendBootstrap/resources/BackendBootstrap.pem ubuntu@ec2-52-90-169-151.compute-1.amazonaws.com

### Para rodar o projeto na AWS

O projeto usa forever.js para rodar o script continuamente na maquina 
(https://github.com/foreverjs/forever)

na pasta do projeto

    forever start server.js
